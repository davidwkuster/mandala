#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Creates a list of N top words, with N given by user input, then creates a json file
with the concurrence of these words with all the other words in the sentence.
Only needs a tweets.csv dataset as input and depends os the lib_text.py lib.
The max children words per input word is given by the user, and the number of
ramifications as well.
"""

import string, csv, os
from lib_text2 import punct_translate_tab as punct_tab
from lib_text2 import dual_mean, filtered, isNumber, word_remove_list, word_start, word_in, clear_text
from lib_text import is_stopword
from lib_text import remove_latin_accents
from lib_text import is_hashtag
from lib_text import is_twitter_mention

def read_db_data(collection, FILTER, projection, text_dict, word_count, ids_dict, repeated_text):
#Reads from a mongo db collection
	print("\nReading DB...\n")

	db_cursor = collection.find(FILTER, projection)
	text_list = []

# +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	txt = 'text'
	idpos = 'id'
	media_count = 0
	tweet_count = 0
	texts = []
	line = {}

	for doc in db_cursor:

		# adapting for new input
		line['text'] = doc['status'].pop('text')
		texts.append(line['text'])

# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# skips repeated text or very similar text
		if not repeated_text:
			if text_in_dataset(line['text'], text_list):
				# print(line['text'])
				# print(text_list)
				continue
			else:
				text_list.append(filtered(line['text']))

		tweet_count += 1

		try:
			line['status']['entities']
			media_count += 1
			line['id'] = 'I' + doc['status'].pop('id_str')
		except KeyError:
			try:
				doc['status']['retweeted_status']['entities']
				media_count += 1
				line['id'] = 'I' + doc['status'].pop('id_str')
			except KeyError:
				line['id'] = doc['status'].pop('id_str')

		tmp2 = clear_text(line[txt])
		text_dict[line[idpos]] = tmp2

		# creates a word count
		# IMPORTANT - all words are stored as lowercase and accentless
		# DECIDE WHETHER THIS IS THE BETTER APPROACH
		for word in tmp2:
			# counts hastags
			if is_hashtag(word):
				try:
					hash_count[word]+=1
				except KeyError:
					hash_count[word] = 1
			# counts users
			elif is_twitter_mention(word):
				try:
					user_count[word]+=1
				except KeyError:
					user_count[word] = 1
			# counts all the other words
			else:
				try: # if id not in ids_dict appends it and adds 1
					 # to count of the word
					 # else adds it and counts
					if line[idpos] not in ids_dict[filtered(word)]:
						ids_dict[filtered(word)].append(line[idpos])
					word_count[filtered(word)] += 1
				except KeyError:
					word_count[filtered(word)] = 1
					ids_dict[filtered(word)] = [line[idpos]]

	db_cursor.close()
	# print(word_count)
	return [tweet_count, media_count, texts]

def text_in_dataset(text, text_list):
	return any(filtered(text) in item or item in filtered(text) for item in text_list)