
from lib_text2 import all_words_in_id, filtered, find_word
from lib_text import is_stopword, is_hashtag, is_twitter_mention
from operator import itemgetter

#Variables
top_words = [] # list of the top words
top_hash = []
top_user = []
concur_list = [] # concurrence list --- output list
json_tree = {} # output for json
stored_concur = []

#DEFINES
MAX_WORDS = 10
MAX_HEIGHT = 10
MAX_DEPTH = 4
MAX_HASH = 100
MAX_USER = 100

separator = ''
PIPE = '|'# default pipe
TAB = '\t' # default tab


# FUNCTIONS
# ============================================================================================================

def check_duplicity(word_list, stored_concur):
	for stored in stored_concur:
		if len(word_list) == len(stored):
			if any(this in stored for this in word_list):
				_max_ = len(stored)
				_count_ = 0
				for word in word_list:
					if word in stored:
						_count_+=1
				if _count_ == _max_:
					if not all(stored[i]==word_list[i] for i in range(len(stored))):
						return True

		# remove 2nd degree duplicates
		elif len(word_list) == len(stored)-1:
			if any(this in stored for this in word_list):
				_max_ = len(stored)-1
				_count_ = 0
				for word in word_list:
					if word in stored:
						_count_+=1
				if _count_ == _max_:
					return True

	# if no duplicate found
	return False



def word_concurrence(word_list, ids_dict, text_dict, duplicity, MAX_WORDS, MAX_DEPTH, MAX_HEIGHT):
	# text_dict per id, ids_dict of words
	# input list of obj words, returns list of obj words that concur with the input list
	# based on the ids of the first word in the list
	global stored_concur
	children_list = []
	local_dict = {}
	root_word = word_list[len(word_list)-1]['name'] # will get the ids of the newest element
	parent_obj = word_list[len(word_list)-1] 		# in the list only

	if parent_obj['size'] > 0: # necessary for requested top_words
		for ids in ids_dict[filtered(root_word)]:
			if all_words_in_id(word_list, ids, text_dict): # checks if word list present in id
				for word in text_dict[ids]: #concur of each word in the id
					if not is_hashtag(word) and not is_twitter_mention(word):
						tmp_dict = {}
						# checks if the word is already in the parent concurrents
						if filtered(word) not in (filtered(root['name']) for root in word_list):
							# counts concurrence up if already in the list
							# or inserts it if not in the list
							
							# concur list of words
							temp_concur = []
							for each in word_list:
								temp_concur.append(filtered(each['name']))
							temp_concur.append(filtered(word))

							if not duplicity:
								skip = check_duplicity(temp_concur, stored_concur)
							else:
								skip = False
							
							# skkiping in case of duplicates not allowed
							if not skip:					
								# creates a checkable list of already performed concurrences
								# in any order
								stored_concur.append(temp_concur)

								# if the word is already in the children_list adds one
								# else creates size with 1
								if any(filtered(word) == filtered(wlocal['name']) for wlocal in children_list):
									tmp = find_word(children_list, word)
									tmp['size'] += 1
									tmp['ids'].append(ids)
									if ids in parent_obj['ids']:
										parent_obj['ids'].remove(ids)
								else:
									tmp_dict['name'] = word
									tmp_dict['size'] = 1
									tmp_dict['ids'] = [ids]
									if ids in parent_obj['ids']:
								 		parent_obj['ids'].remove(ids)

									children_list.append(tmp_dict)

	# returns list sorted out by size of the items
	# recurssion implementation
	if children_list:
		# print(word_list)
		# print(children_list)
		return_list = sorted(children_list, key=itemgetter('size'), reverse=True)
		return_list = return_list[0:MAX_HEIGHT] # cuts the list into the maximum
												# number of top words
		for obj in return_list:
				# as all concurrent words to a list
				# necessarily are not in that list ...
				tmp = [] #creates clear temp list
				tmp = list(word_list) #sets new local list of from the input word sequence
				tmp.append(obj)

				if len(tmp) < MAX_DEPTH or MAX_DEPTH == 0: # for the non-evaluated sequences, if not bigger the max recurssion depth
					obj['children'] = word_concurrence(tmp, ids_dict, text_dict, duplicity, MAX_WORDS, MAX_DEPTH, MAX_HEIGHT) # does the recurssion for that new sequence
				else: # if reaches bottom no more children allowed
				# MODIFIED IDS QUANTITY
					# obj['ids'] = obj['ids'][0:MAX_IDS] # last item carries the ids, thus cut out the excess here
					obj['children'] = None
		return return_list
	else:
		return None