#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Processes requests for concurrence.
"""

from lib_concur import check_duplicity, word_concurrence,top_words, \
					   top_hash,top_user,concur_list,json_tree, \
					   stored_concur,MAX_WORDS,MAX_DEPTH, \
					   MAX_HASH,MAX_USER,separator,PIPE,TAB
from lib_text2 import filtered

from lib_IO import *
import json

def OptionParser():
	global separator, input_file, option, MAX_WORDS, MAX_HASH, MAX_USER
	# while separator not in ['pipe', 'tab']:
	input_sep = input('\nFile separator:   (tab or pipe)\n> ')
	if input_sep == 'tab':
		separator = TAB
	else:
		separator = PIPE
	print('Using separator: ' + separator)

	option = ''
	while option not in ['top', 'concur', 'sankey']:
		option = input('\nType Option:\n> top\n> concur\n> sankey\n-------\n> ')

	# number of top words
	try:
		MAX_WORDS = int(input('\nSet the MAX # of TOP WORDS.\n(e.g \'10 words\')> '))
	except ValueError:
		print('\nSet to default: ' + str(MAX_WORDS))
	
	# number of top hashtags
	try:
		MAX_HASH = int(input('\nSet the MAX # of HASHTAGS.\n(e.g \'10 #\')> '))
	except ValueError:
		print('\nSet to default: ' + str(MAX_HASH))
	
	# number of mentions
	try:
		MAX_USER = int(input('\nSet the MAX # of TOP MENTIONS.\n(e.g \'10 @\')> '))
	except ValueError:
		print('\nSet to default: ' + str(MAX_USER))

	# get filenames
	input_file = input("\nEnter filename\n(e.g. tweets.csv or face.tab)> ")

#======================================================================================
# gets actual concurrence list of words
def concur_top_words(top_words, duplicity):
	# global text_dict, word_count, ids_dict
	# global MAX_WORDS, MAX_HEIGHT, MAX_DEPTH
	json_tree_return = {}
	concur_list = []
	used_ids = []

	top_words = top_words[0:MAX_WORDS]
	print('Processing concur...')
	for i in range(len(top_words)):
		tmp_dict = {}
		tmp_dict['name'] = top_words[i][0]
		tmp_dict['size'] = top_words[i][1]

		# +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		# remove duplicated ids from top_words
		for _id in ids_dict[filtered(tmp_dict['name'])]:
			if not _id in used_ids:
				used_ids.append(_id)
			else:
				ids_dict[tmp_dict['name']].remove(_id)

		if tmp_dict['size'] > 0:
			tmp_dict['ids'] = ids_dict[tmp_dict['name']]
			tmp_dict['children'] = word_concurrence([tmp_dict], ids_dict,text_dict, duplicity, MAX_WORDS, MAX_DEPTH, MAX_HEIGHT)
		else:
			tmp_dict['children'] = None
			tmp_dict['ids'] = None

		concur_list.append(tmp_dict)

	# root of the tree, needed in the Viz
	json_tree_return['name'] = 'top_words'
	json_tree_return['children'] = concur_list

	return json_tree_return

# gets actual concurrence list of words
def concurrent_list(word_list, duplicity):
	json_tree_return = {}
	concur_list = []

	# concurs the first
	json_tree = concur_top_words([word_list[0], count_word_size(word_list[0], ids_dict)], duplicity)

	return json_tree

def return_word_children(word, child):
	return word['children'][child]

def count_word_size(word, ids_dict):
	try:
		count = len(ids_dict[filtered(word)])
	except KeyError:
		count = 0

	return count

def GetMAX():
	global MAX_WORDS, MAX_HEIGHT, MAX_DEPTH
	# MAXIMUMS
	try:
		MAX_WORDS = int(input('\nSet the MAX # of TOP WORDS.\n(e.g \'100 words\')> '))
	except ValueError:
		MAX_WORDS = 15
		print('\nSet to default: ' + str(MAX_WORDS))
	try:
		MAX_HEIGHT = int(input('\nSet the number of concurrent words.\n(e.g \'100 top concurrent words)\n> '))
	except ValueError:
		print('\nSet to default: ' + str(MAX_HEIGHT))
	try:
		MAX_DEPTH = int(input('\nSet the MAX DEPTH of ramifications.\n(\'0\' means no limit\n!!!which should take a HECK a lot o\' time!!!!\n IF YOU ARE NOT SURE SET TO -1-)\n> '))
	except ValueError:
		print('\nSet to default: ' + str(MAX_DEPTH))


def Output_json(out_file,folder,json_tree):
	print("\nWriting JSON...\n")
	out_file = open(os.path.abspath(os.path.join(os.path.dirname( __file__ ), folder, output_file)),'w')
	json.dump(json_tree, out_file, indent=4)

def Output_csv(output_file, concur_list):
	print("\nWriting one-level concur to csv...\n")

	buffer_out = []
	local_list = []
	
	buffer_out.append(['word','size', 'concurrence', 'size', '>>>']) # header

	# concurrence for each top word
	# in a plain csv file
	for word in concur_list:
		# appends new line 'list'
		buffer_out.append([])
		# appends word to last pos of buffer_out
		buffer_out[len(buffer_out)-1].append(word['name'])
		buffer_out[len(buffer_out)-1].append(word['size'])
		# append children
		children = word['children']
		# if has children
		if children:
			for child in children:
				buffer_out[len(buffer_out)-1].append(child['name'])
				buffer_out[len(buffer_out)-1].append(child['size'])
	if not output_file.endswith('.csv'):
		output_file = output_file + '.csv'
	mkcsv(buffer_out, os.path.abspath(os.path.join(os.path.dirname( __file__ ), '..', output_file)))

def ParseSankey(concur_list, sankey):
	for item in concur_list:

		new_node = {'name' : item['name']}
		if new_node not in sankey['nodes']:
			sankey['nodes'].append(new_node)

		children = item['children']

		if children:
			for child in children:

				new_node = {'name' : child['name']}
				if new_node not in sankey['nodes']:
					sankey['nodes'].append(new_node)
			
				sankey['links'].append({ 'source' : item['name'],
									  'target' : child['name'],
									  'value'  : child['size']})

			if child['children']:
				sankey = ParseSankey(child['children'], sankey)
	return sankey

# =====================================================================================================
# Main part
if __name__ == '__main__':
	# get options
	OptionParser()

	# open tweets csv
	opencsv(input_file, separator)

	# process tops
	print('\nProcessing...\n')
	# tuplelist for getting the top words
	for word in word_count:
		top_words.append([word, word_count[word.lower()]])
	top_words = sorted(top_words, key=lambda x: x[1], reverse=True)
	top_words = top_words[0:MAX_WORDS] # gets maximum number of top words

	# Top Words
	for hashtag in hash_count:
		top_hash.append([hashtag, hash_count[hashtag]])
	top_hash = sorted(top_hash, key=lambda x: x[1], reverse=True)
	top_hash = top_hash[0:MAX_HASH]

	# Top Mentions
	for mention in user_count:
		top_user.append([mention, user_count[mention]])
	top_user = sorted(top_user, key=lambda x: x[1], reverse=True)
	top_user = top_user[0:MAX_HASH]

	top_word_path = 'top_words_' + input_file
	if not top_word_path.endswith('.csv'):
		top_word_path = top_word_path + '.csv'
	mkcsv(top_words, os.path.abspath(os.path.join(os.path.dirname( __file__ ), '..', top_word_path)))

	top_hash_path = 'top_hash_' + input_file
	if not top_hash_path.endswith('.csv'):
		top_hash_path = top_hash_path + '.csv'
	mkcsv(top_hash, os.path.abspath(os.path.join(os.path.dirname( __file__ ), '..', top_hash_path)))

	top_user_path = 'top_mentions_' + input_file
	if not top_user_path.endswith('.csv'):
		top_user_path = top_user_path + '.csv'
	mkcsv(top_user, os.path.abspath(os.path.join(os.path.dirname( __file__ ), '..', top_user_path)))

	if option == 'top':
		if input('Do you want to make the word concur for ' + input_file + ' now?\n(y/n)> ') in ['y', 'Y']:
			option = 'concur'
		else:
			exit()

	if option == 'concur' or option == 'sankey':
		# output filename
		output_file = "concur_" + input_file
		# max concurrence options
		GetMAX()

		duplicity = input('\nKeep Duplicates? [y/n]\n> ') in ['y','Y']
		if duplicity:
			print('\nKeeping out-of-order duplicates.\n')
		else:
			print('\nNo Duplicates.\n')

		# CONCUR
		json_tree = concur_top_words(top_words, duplicity)

		if option == 'concur':
			# writes files
			# prints out as csv file
			output_file = 'concur_' + input_file
			if not (output_file.endswith('.csv')):
				output_file = output_file + '.csv'

			Output_csv(output_file, concur_list)

			output_file = 'concur_' + input_file
			if not (output_file.endswith('.json')):
				output_file = output_file + '.json'
			# prints json
			Output_json(output_file,'..',json_tree)

	if option == 'sankey':
		print('\n=]\n')
		sankey = {}
		sankey['links'] = []
		sankey['nodes'] = []

		sankey = ParseSankey(concur_list, sankey)

		output_file = 'sankey'
		if not output_file.endswith('.json'):
			output_file = output_file + '.json'
		# prints json
		Output_json(output_file,'..',sankey)

def parse_concur(collect, FILTER):
	from json import dumps, loads
	from bottle import request
	from lib_db_IO import read_db_data
	import datetime

	import sys
	#db config path
	sys.path.append('../../../')

	# from cache_generator import 
	global text_dict, word_count, ids_dict, json_tree, stored_concur

	# clears out - global vars -
	text_dict.clear()
	word_count.clear()
	ids_dict.clear()
	json_tree.clear()
	stored_concur.clear()
	top_words = []


	# MAXIMUMS
	global MAX_WORDS, MAX_HEIGHT, MAX_DEPTH
	# for top words
	used_ids = []

	# Dictionary for returning Data
	return_dict = {}

	# Default Code
	code = 200
	message = 'Done'

	try:

		# MAXIMUMS

		# print(FILTER)

		# parses the internal settings
		try:
			MAX_WORDS = FILTER.pop('MAX_WORDS')
		except Exception:
			MAX_WORDS = 15
		try:
			MAX_HEIGHT = FILTER.pop('MAX_HEIGHT')
		except Exception:
			MAX_HEIGHT = 15
		try:
			MAX_DEPTH = FILTER.pop('MAX_DEPTH')
		except Exception:
			MAX_DEPTH = 3

		try:
			top_words = FILTER.pop('top_words')
		except Exception:
			pass

		try:
			word_list_concur = FILTER.pop('word_list')
		except Exception:
			word_list_concur = []

		try:
			duplicity = (FILTER.pop('duplicity') == 'true')
		except Exception:
			duplicity = True

		try:
			rt = (FILTER.pop('rt') == 'true')
		except Exception:
			rt = True


		# ++++++++++++++++++++++++++++++++++++++++++++
		try:
			repeated_text = (FILTER.pop('repeated_text') == 'true')
		except Exception:
			repeated_text = True

		FILTER = FILTER['where']

		# don't use rts
		if not rt:
			FILTER['status.retweeted_status'] = {'$exists': False}
		
		try:
			FILTER['status.created_at']['$gte'] = datetime.datetime.strptime(FILTER['status.created_at']['gte'], '%Y-%m-%dT%H:%M:%S.%f')
			FILTER['status.created_at']['$lte'] = datetime.datetime.strptime(FILTER['status.created_at']['lte'], '%Y-%m-%dT%H:%M:%S.%f')
			FILTER['status.created_at'].pop('gte')
			FILTER['status.created_at'].pop('lte')

		except Exception as why:
			code = 400
			message = 'Invalid argument for Date: bad format or missing element.'
			raise NameError(str(why))

		try:
			FILTER['categories']['$in'] = FILTER['categories'].pop('inq')
		except Exception:
			pass
		
		try:
			FILTER['categories']['$all'] = FILTER['categories'].pop('all')
		except Exception:
			pass

		print('\nParsing: ' + str(FILTER['status.created_at']['$gte']) + ' - ' + str(FILTER['status.created_at']['$lte']))
		
		# sets a projection to return
		projection = {'status.text': 1, 'status.id_str': 1, '_id': 0, 'status.entities.media.media_url_https' : 1, 'status.retweeted_status.entities.media.media_url_https' : 1 }
		# added images


		try:
			counts = read_db_data(collect, FILTER, projection, text_dict, word_count, ids_dict, repeated_text)

			# ++++++++++++++++++++++++++++++++++++++++++++++++++
			# check cache first


			# ++++++++++++++++++++++++++++++++++++++++++++++++++
			# process concur
		
			# if no top words received in the filter, process top words from the dataset
			if not top_words and not word_list_concur:
				top_words = []
				for word in word_count:
					top_words.append([word, word_count[word.lower()]])
					# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++
					# remove duplicated ids
					for _id in ids_dict[filtered(word)]:
						if not _id in used_ids:
							used_ids.append(_id)
						else:
							ids_dict[filtered(word)].remove(_id)
					# print(word)
					# print(word_count[filtered(word)])


				top_words = sorted(top_words, key=lambda x: x[1], reverse=True)
				top_words = top_words[0:MAX_WORDS] # gets maximum number of top words
			# if top words received and not concur list
			elif not word_list_concur:
				tmp = []
				for word in top_words:
					tmp.append([word,count_word_size(word,ids_dict)])
				top_words = tmp
			else: # In case a word list is given
				print('Works!!!')
				concurrent_list(word_list_concur, duplicity)

			json_tree = concur_top_words(top_words, duplicity)
			
			return_dict['data'] = json_tree

		except Exception as _why:
			print('DB Error: ' + str(_why))
			code = 408
			message = 'RequestTimeout'

	except Exception as why:
		print('Error: ' + str(why))
		code = 400
		message = 'BadRequest: ' + str(why)
	
	if code != 200:
		return_dict['meta'] = { 'code': code, 'message': message}
		return return_dict['meta']
	else:
		return_dict['data']['media_count'] = counts[1]
		return_dict['data']['tweet_count'] = counts[0]
		# return_dict['data']['texts'] = counts[2]

		return return_dict['data']

def parse_local_concur(collect, FILTER):
	from json import dumps, loads
	from bottle import request
	from lib_db_IO import read_db_data
	import datetime

	import sys
	#db config path
	sys.path.append('../../../')

	# from cache_generator import 
	global text_dict, word_count, ids_dict, json_tree, stored_concur

	# clears out - global vars -
	text_dict.clear()
	word_count.clear()
	ids_dict.clear()
	json_tree.clear()
	stored_concur.clear()
	top_words = []


	# MAXIMUMS
	global MAX_WORDS, MAX_HEIGHT, MAX_DEPTH
	# for top words
	used_ids = []

	# Dictionary for returning Data
	return_dict = {}

	# Default Code
	code = 200
	message = 'Done'

	try:

		# MAXIMUMS

		# print(FILTER)

		# parses the internal settings
		try:
			MAX_WORDS = FILTER.pop('MAX_WORDS')
		except Exception:
			MAX_WORDS = 15
		try:
			MAX_HEIGHT = FILTER.pop('MAX_HEIGHT')
		except Exception:
			MAX_HEIGHT = 15
		try:
			MAX_DEPTH = FILTER.pop('MAX_DEPTH')
		except Exception:
			MAX_DEPTH = 3

		try:
			top_words = FILTER.pop('top_words')
		except Exception:
			pass

		try:
			word_list_concur = FILTER.pop('word_list')
		except Exception:
			word_list_concur = []

		try:
			duplicity = (FILTER.pop('duplicity') == 'true')
		except Exception:
			duplicity = True

		try:
			rt = (FILTER.pop('rt') == 'true')
		except Exception:
			rt = True


		# ++++++++++++++++++++++++++++++++++++++++++++
		try:
			repeated_text = (FILTER.pop('repeated_text') == 'true')
		except Exception:
			repeated_text = True

		FILTER = FILTER['where']

		# don't use rts
		if not rt:
			FILTER['status.retweeted_status'] = {'$exists': False}
		
		try:
			FILTER['status.created_at']['$gte'] = datetime.datetime.strptime(FILTER['status.created_at']['gte'], '%Y-%m-%dT%H:%M:%S.%f')
			FILTER['status.created_at']['$lte'] = datetime.datetime.strptime(FILTER['status.created_at']['lte'], '%Y-%m-%dT%H:%M:%S.%f')
			FILTER['status.created_at'].pop('gte')
			FILTER['status.created_at'].pop('lte')

		except Exception as why:
			code = 400
			message = 'Invalid argument for Date: bad format or missing element.'
			raise NameError(str(why))

		try:
			FILTER['categories']['$in'] = FILTER['categories'].pop('inq')
		except Exception:
			pass
		
		try:
			FILTER['categories']['$all'] = FILTER['categories'].pop('all')
		except Exception:
			pass

		print('\nParsing: ' + str(FILTER['status.created_at']['$gte']) + ' - ' + str(FILTER['status.created_at']['$lte']))
		
		# sets a projection to return
		projection = {'status.text': 1, 'status.id_str': 1, '_id': 0, 'status.entities.media.media_url_https' : 1, 'status.retweeted_status.entities.media.media_url_https' : 1 }
		# added images


		try:
			counts = read_db_data(collect, FILTER, projection, text_dict, word_count, ids_dict, repeated_text)

			# ++++++++++++++++++++++++++++++++++++++++++++++++++
			# check cache first


			# ++++++++++++++++++++++++++++++++++++++++++++++++++
			# process concur
		
			# if no top words received in the filter, process top words from the dataset
			if not top_words and not word_list_concur:
				top_words = []
				for word in word_count:
					top_words.append([word, word_count[word.lower()]])
					# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++
					# remove duplicated ids
					for _id in ids_dict[filtered(word)]:
						if not _id in used_ids:
							used_ids.append(_id)
						else:
							ids_dict[filtered(word)].remove(_id)
					# print(word)
					# print(word_count[filtered(word)])


				top_words = sorted(top_words, key=lambda x: x[1], reverse=True)
				top_words = top_words[0:MAX_WORDS] # gets maximum number of top words
			# if top words received and not concur list
			elif not word_list_concur:
				tmp = []
				for word in top_words:
					tmp.append([word,count_word_size(word,ids_dict)])
				top_words = tmp
			else: # In case a word list is given
				print('Works!!!')
				concurrent_list(word_list_concur, duplicity)

			json_tree = concur_top_words(top_words, duplicity)
			
			return_dict['data'] = json_tree

		except Exception as _why:
			print('DB Error: ' + str(_why))
			code = 408
			message = 'RequestTimeout'

	except Exception as why:
		print('Error: ' + str(why))
		code = 400
		message = 'BadRequest: ' + str(why)
	
	if code != 200:
		return_dict['meta'] = { 'code': code, 'message': message}
		return return_dict['meta']
	else:
		return_dict['data']['media_count'] = counts[1]
		return_dict['data']['tweet_count'] = counts[0]
		# return_dict['data']['texts'] = counts[2]

		return return_dict['data']
