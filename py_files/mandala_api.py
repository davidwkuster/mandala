#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
"""
from bson import json_util
from concur_all_words import parse_local_concur

import bottle
from bottle import route, run, template, get, request, post, response

# the decorator
def enable_cors(fn):
    def _enable_cors(*args, **kwargs):
        # set CORS headers
        response.headers['Access-Control-Allow-Origin'] = '*'
        response.headers['Access-Control-Allow-Methods'] = 'GET, POST, PUT, OPTIONS'
        response.headers['Access-Control-Allow-Headers'] = 'Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token'
 
        if bottle.request.method != 'OPTIONS':
            # actual request; reply with the actual response
            return fn(*args, **kwargs)
 
    return _enable_cors

# ========================================================================================================================
@route('/')
def mandala_home():
    from bottle import static_file, response, Response
    # head = response.headers['Access-Control-Allow-Origin'] = '*'   , headers={'Access-Control-Allow-Origin': '*'}
    return static_file('mandala_home.html', root='../mandala')

@route('/mandala')
@enable_cors
def word_concur_route():
	from json import dumps

	response = parse_concur()
	return dumps({"Message":"Works!!!"}, indent=4, default=json_util.default)

# =======================================================================
if __name__ == "__main__":
	run(host='0.0.0.0', port=1234)