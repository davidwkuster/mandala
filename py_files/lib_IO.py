#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""

"""
import string, csv, os
from lib_text2 import punct_translate_tab as punct_tab
from lib_text2 import dual_mean, filtered, isNumber, word_remove_list, word_start, word_in, clear_text
from lib_text import is_stopword
from lib_text import remove_latin_accents
from lib_text import is_hashtag
from lib_text import is_twitter_mention

text_dict = {}
word_count = {} # for top words
user_count = {}
hash_count = {}
ids_dict = {} # ids dict

def opencsv(input_file, separator):
#Reads the RAW csv file
	global text_dict, word_count,ids_dict, hash_count, user_count
	skipped_errors = 0
	print("\nReading " + input_file + "...\n")

	try:
		with open(os.path.abspath(os.path.join(os.path.dirname( __file__ ), '..', input_file)), 'rt', encoding="utf8") as csvfile:
			# position of text in the line
			csv_in = csv.reader(csvfile, delimiter=separator, quoting=csv.QUOTE_MINIMAL, quotechar='"')
			# header
			header = next(csv_in)
			# set positions
			txt = -1
			idpos = -1
			for i in range(len(header)):
				if header[i] in ['id']:
					idpos = i
				if header[i] in ['text']:
					txt = i
			if txt == -1 or idpos == -1:
				print('\nCheck your input file!!!\n' + \
					'Make sure your header has the items \'id\' and \'text\' in it, then try again.\n' + \
					'These are mandatory for the program to run.\n')
				exit()

			for line in csv_in:
				# clears punctuation
				# tmp = "".join(c for c in line[0] if c not in punct)
				if line:
					tmp2 = clear_text(line[txt])
					# tmp = line[txt].translate(punct_tab)
					# tmp = tmp.split()
					# # splits words and checks for stopwords
					# tmp2 = []
					# for word in tmp:
					# 	## clears the text from irrelevant puctuation and commom words
					# 	if not (is_stopword(word.lower()) and word not in dual_mean) \
					# 	and not any(w in word for w in word_in)\
					# 	and not any((word.startswith(w) or word == w) for w in word_start) \
					# 	and word.lower() not in word_remove_list and not isNumber(word):
					# 		tmp2.append(word)
					# # saves the list of words per id
					try:
						text_dict[line[idpos]] = tmp2
					except Exception as why:
						print("Skiping broken Line: ")
						print(line)
						print('Error: ' + str(why))
						if input('\nContinue?\n(y/n)> ') not in ['y','Y']:
							print('Fix the input file and try again.\n\n\n')
							exit()
						skip += 1
						continue # jumps for next line

					# creates a word count
					# IMPORTANT - all words are stored as lowercase and accentless
					# DECIDE WHETHER THIS IS THE BETTER APPROACH
					for word in tmp2:
						# counts hastags
						if is_hashtag(word):
							try:
								hash_count[word]+=1
							except KeyError:
								hash_count[word] = 1
						# counts users
						elif is_twitter_mention(word):
							try:
								user_count[word]+=1
							except KeyError:
								user_count[word] = 1
						# counts all the other words
						else:
							try: # if id not in ids_dict appends it and adds 1
								 # to count of the word
								 # else adds it and counts
								if line[idpos] not in ids_dict[filtered(word)]:
									ids_dict[filtered(word)].append(line[idpos])
								word_count[filtered(word)] += 1
							except KeyError:
								word_count[filtered(word)] = 1
								ids_dict[filtered(word)] = [line[idpos]]

		print(str(len(text_dict)) + " tweets read from \'" + input_file + "\'.\n" + str(len(word_count)) + " words read.\n")
		if skipped_errors:
			print('# of Errors found: ' + str(skipped_errors))
	except IOError:
		print(input_file + ": No such file or directory.\n")
		exit()

def mkcsv(buffer_out, output_file):
	# Dumping buffer out to file *.csv
	with open(output_file, 'w', newline='', encoding="utf8") as csvfile:
		writer = csv.writer(csvfile, delimiter='|', quotechar='"', quoting=csv.QUOTE_MINIMAL)

		for line in buffer_out:
			writer.writerow(line)