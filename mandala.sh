# /bin/bash

# runnable for the concur words

cd ./py_files
time python3 concur_all_words.py

cd ../




if [ ! -d "result" ] ; then 
	echo "Creating result folder."
	mkdir result

fi

if [[ -n $(ls top*) ]] ; then
	if [ ! -d "Top" ]; then
		echo "Creating Top directory."
		mkdir Top
	fi

	cp -r top* ./Top
	rm -rf top*
	echo "Moved top files to Top dir"
	cp -r Top result
	rm -r Top
else
	echo ""
	echo "No top Found"
	echo ""
fi

if [[ -n $(ls concur_*) ]]; then
	if [ ! -d "Concur" ]; then
		echo "Creating Concur directory"
		mkdir Concur
	fi

	echo "Moving concur files to Concur dir." 
	cp -r concur_* ./Concur
	rm -rf concur_*
	cp -r Concur result
	rm -rf Concur
else
	echo ""
	echo "No Concur Found."
fi

	echo ""
	echo "Finished !!! "
	echo ""

cp sankey.json ./result
rm -rf sankey.json

exit 0
